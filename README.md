# aito - ai to brighten your creation #

## Recent changes ##

WARNING: We are preparing an initial API freeze, and we are making changes that may
break api compatibility. We're striving to finish the work as soon as possible and
to limit the changes

## API changes

Note, if you want to update instance to the newest version, send mail to kai@aito.ai or antti@aito.ai. After the update: please erase and recreate the schema and repopulate the database, because the update may make the database binary incompatible. We will handle the binary compatibility issues later. 

1. The versions deployed after 27.4.2018 have following additions:
    1. Relate API is refactored: 
        * Relate now returns 10 relations by default! You can get more relations by using 'take'
          and 'from' parameters to provide pagination with relate query. 
        * Filter argument can be used to manage the relate sensitivity.
        * to-arguments now accept wide range of operations, instead of plain paths
    1. Or-operation is now supported. And operation works in wider situations. 
    1. Variable-operation has been exposed. Variable-operation can be used to turn And operations
       into And-variables.
    1. byHitScore and byContextScore are exposed for sorting by non-probability scores.
    1. Lift, Product, Exponent and SumTo operations are exposed for combining scores from e.g. similarity
    1. Match operation is supported in some {"field":"x", "matches":"foo bar"} cases
    1. Now 'whereHit' and 'whereContext' operation supports generic query as the parameter
    1. Similarity updated with more correct normalization operations
    1. Now 'when' supports generic query as the parameter
1. The versions deployed after 4.4.2018 have following additions 
    1. Optional variables now have two different feature fields named 'defined' and 'value'. 
       For example a conditional field antennaType will have two feature fields named antenna.defined
       and antenna.value. antenna.defined has always a boolean values, e.g. antenna.defined:true, 
       while antenna.value will contain the field features.
    1. Conditional comparisons are now supported. Now, the relation's statistical comparisons
       only take into account rows, where all the examined features are defined. For example:
       if we have 'vehicles' table with optional property antennaType and non-optional property
       'manufacturer', the relation e.g. antennaType.value:'embedded' and manufacturer:'acme'
       is only counted for rows, for which antennaType is not null/none and antennaType.defined is true.
    1. The values behind optional links are undefined, if the linking optional value is undefined.
1. The versions deployed after 26.3.2018 have following additions
    1. Less verbose path name syntax:
        * Previously, the column paths were noted with javascript arrays, e.g.
        ```
          {"when":["customer", "tags"], "is":"20s"}
        ```
        * Now, you can use plain strings, and also use 'dot ' notation, e.g.
        ```
          {"when":"customer.tags", "is":"20s"}
        ```
        * This alternative syntax was added to reduce the verbosity of the API
    1. OrderByContextProbability methods no more need to write down the context name.
        * Previously, you would need to write down, that you are refererring the impression context, e.g.
        ```
          {"orderByContextProbability":["impressions","click"], "has":true}
        ```
        * Now the context name can be omitted, e.g.
        ```
        {"orderByContextProbability":{"field":["click"], "has":true}}
        ```
        * You can also use string paths instead of arrays: e.g.
        ```
        {"orderByContextProbability":{"field":"click", "has":true}}
        ```
    1. Similarity and And operations were added. See more in the latter documentation
    1. Some error message improvements (e.g. if column is not found, a special column error message is prompted)
  1. Version 158 and later, have the file/stream APIs for data upload
  1. Version 202 and  later have an improved schema API, supporting GET-requests to show the schema both for the entire 
    database, as well as for individual tables. The changes are breaking, and require re-provisioning the existing schema. 


## API compatibility violations

1. 4.4.2018 Api compatibility was broken for 'has' method in case of optionals. 
   For field x, the field feature f is no more named x:f, but x.value:f. 
1. 26.3.2018 Api compatibility was broken in orderByContextProbability method.
    * orderByContextProbability no more takes 'has parameter'. Instead it takes 'field has' structure.
    * Old version was:
    ```
    {"orderByContextProbability":["impressions","click"], "has":true}
    ```
    * New version is:
    ```
    {"orderByContextProbability":{"field":"click", "has":true}}
    ```

## Disclaimer ##

aito is still in Pilot phase. The APIs are not yet frozen, and they are still subject to change.
There are bugs in the product, and there are entire sections of the software stack, that are not yet
properly tested. Some functionality may not even work in a sensible way. Still, some parts of the
software stack may be surprisingly fast, robust and well functioning.

See quality section for more information.

## Introduction ##

aito is an intelligence layer that provides you instant AI functionality through queries.

aito can provide you instantly arbitrary:

 * predictions and estimates
 * smart searches, personalization and matches
 * analytics and diagnostics
 * and traditional sql operations

For example, aito can provide the answer to the following query:

    {
        "context":"impressions",
        "q":[{"when":"customer", "is":5},
             {"when":"query",    "is":"quality 13 inch laptop"},
             {"when":"page",     "is":"laptops"},
             {"find":"product"},
             {"whereHit":"type", "is":"laptop"},
             {"orderByContextProbability":{"field":"click", "has":true}}
    }

The query will return all products, which 'type' field is 'laptop', sorted by their
click probability for the given user, query and page.

These 'smart queries' are enabled by deep AI technology and database technology integration.
By integrating the AI deeply with the database operations, aito can get rid of ipc and integration overheads. By specializing the database for fast statistical operations, aito makes it possible
to perform thousands of statistical operations real-time, which enables creating real-time ad-hoc
models to answer the arbitrary queries.

## Getting started ##

The examples presented in document can be found in executable format in the [ecommerce test repository](https://bitbucket.org/aito_ai/aitoai-ecommerce-test).

### 1. Getting a Pilot-program aito end point & API key ###

First of all, you need an dedicated end-point and an API key to use aito. The dedicated end-points
and API keys are provided by Kai Inkinen.

We currently provide end-points and keys to our pilot customers. If you are not part of our
pilot program, please contact Vesa-Pekka "Vesku" Gr�nfors. We're planning to open public trials as soon as the API-format
and functionality for it has been finalised.

Please check out [known issues](#markdown-header-known-issues) at the bottom of this document before starting.

### 1.1 Feedback, reporting bugs or inconsistencies

We take quality seriously and aim to perfect our software both for errors as well as usability issues. However, we're still in pilot phase, so we're making improvements and tweaks both to the core-functionality as well as the APIs. This means that sooner or later something
is bound to go out or order.

If you run into problems, please let us know about it, and we'll fix it ASAP. You can contact us on:

* Email: You can send bug reports to quality@aito.ai, please include version information and reproduction steps in the email. Please also leave your contact details, so we can be in contact with you for more information, or if you want to be notified on the status.

* Slack. We're opening a [public slack channel](https://aitoai.slack.com), where you can report bugs, ask questions or in general discuss anything product related. Please send and email with your contact details to slack@aito.ai to receive an invitation. Email domains aito.ai and futurice.com are accepted by default.


### 2. Populating aito

Aito uses JSON as the data format. The incoming data, as well as responses from Aito are encoded using the `Content-Type: application/json; charset=utf-8`. Aito requires the incoming requests to define the data type in the header,
and will return 404 unless it has been properly set. This applies for all API-calls listed below.

The API is documented according to [OpenAPI 3.0 Specification](https://swagger.io/specification/), using [Swagger](https://swagger.io/). The documentation is available alongside the API, at the the address [https://aito.ai/openapi/index.html](https://aito.ai/openapi/index.html).

The current version information of the database can be checked from the URL [https://aito-host/healthcheck] . When reporting bugs or issues, or having questions, please attach the version information with your request.

#### 2.1 Defining schema

aito resembles closely both databases and search indexes.

aito resembles datatbases in the sense, that the data is stored in cross-linked tables following a strict
schema. On the other hand, aito resembles search engines in the sense, that the various columns/fields need
analyzers for splitting the values into 'features'.

Let's consider following example. We have a dataset consisting of customers, products and impressions. Before  populating the data, we need to define a schema.

The schema definition is done by sending a HTTP PUT request to address [https://aito-host/api/schema/_mapping], containing following data

      {
        "tables": [
          {
            "table": "customers",
            "columns": [
              { "column": "id",   "type": "Int" },
              { "column": "name", "type": "String" },
              { "column": "tags", "type": "String", "analyzer": "Whitespace" }
            ]
          },{
            "table": "products",
            "columns": [
              { "column": "id",          "type": "Int" },
              { "column": "title",       "type": "String",  "analyzer": "English" },
              { "column": "tags",        "type": "String",  "analyzer": "Whitespace" },
              { "column": "price",       "type": "Decimal", "analyzer": "No" },
              { "column": "description", "type": "String",  "analyzer": "English" }
            ]
          },{
            "table": "impressions",
            "columns": [
              { "column": "prevProduct", "type": "Optional(Int)", "link": "Optional(products.id)" },
              { "column": "customer",    "type": "Int",           "link": "customers.id" },
              { "column": "product",     "type": "Int",           "link": "products.id" },
              { "column": "query",       "type": "String",        "analyzer": "English" },
              { "column": "click",       "type": "Boolean" }
            ]
          }
        ]
      }

Above JSON describes an aito schema with 3 cross-linked tables. The tables are defined throught a list
of column definitions. Each column definition defines least the column name, and the column type. There are currently 10 different column types supported, that are:

* Boolean and Optional(Boolean)
* Int and Optional(Int)
* Decimal and Optional(Decimal)
* String and Optional(String)

The optional versions values can be omitted.

The column values are turned into features by analyzers. If analyzer is not defined, the default analyzer is used. Following analyzers are currently supported:

* 'Default' - this mechanism is used by default. It turns the field value directly into a feature without splitting it into smaller parts. For example, using default analyzer for text 'code 435 - heat' for field alert will create a feature alert:'code 435 - heat' without splitting the text into smaller parts.
* 'No' - is used to avoid analysis entirely. Use this if - for a reason or another - you do not want aito to use this data in reasoning. This may make sense with numbers, which will be better supported in the future.
* 'Whitespace' splits text by whitespace. E.g. whitespace analyzer would split tag field 'laptop ios premium'  into features tag:laptop, tag:ios and tag:premium
* 'Standard' analyzer - can be used to treat text. Standard analyzer will omit punctuation like ., ! and ?, and it can analyze web and email addresses correctly
* 'Finnish' analyzer is for treating Finnish text. It will turn morphologies like 'kissat' and 'kissoja' into their base form
* 'English' analyzer is for treating English text. It will turn morphologies like 'cats' and 'running' into their base form
* 'Swedish' analyzer is for treating Swedish text.
* 'German' analyzer is for treating German text.

The link properties describe cross-table links. The links can point to the same table, or another table. The links are symbolic, they always point to a specific field in target table, and they are resolved using the index. This means, that the link field and the linked target field should be of the same type, and they should be analyzed in a compatible manner (prefer default analyzers for the link field and the linked field).

Two different link types are support, that are:

* Normal link types, e.g. "link":"table.field". Normal links contain always a link
* Optional link types, e.g. "link":"Optional(table.field)". Optional links may or may not contain a link. The optional links go hand-hand with optional types in link field and non-optional types in linked field. E.g. Optional(Int) column may optinally link to Int column, and Optional(String) column may optionally link to String column.

#### 2.2 Populating the tables

You can populate the defined tables by sending a POST request to address of form https://aito-host/api/data/TABLE/batch contain the JSON data.

For example the previous customers table can be populated by sending following JSON to address https://aito-host/api/data/customers/batch:

    [
        {"id":0, "name":"anne",     "tags":"london 20s"},
        {"id":1, "name":"bob",      "tags":"nyc 20s"},
        {"id":2, "name":"cecilia",  "tags":"nyc 30s"},
        {"id":3, "name":"david",    "tags":"london 30s"},
        {"id":4, "name":"elisabeth","tags":"london 40s"},
        {"id":5, "name":"felix",    "tags":"nyc 40s"},
        {"id":6, "name":"grace",    "tags":"nyc 50s"},
        {"id":7, "name":"harald",   "tags":"london 50s"},
        {"id":8, "name":"iris",     "tags":"nyc 60s"}
    ]

Similarly, we can populate products table with POST request to /api/data/products/batch containing:

    [
        { "id":0,  "title":"apple iphone",              "tags":"ios phone premium",         "price":800.0, "description":"apple iphone is a premium phone compatible with all laptops"},
        { "id":1,  "title":"samsung s8",                "tags":"android phone premium",     "price":600.0, "description":"samsung s8 is the top phone with all sorts of features "},
        { "id":2,  "title":"huawei honor",              "tags":"android phone affordable",  "price":150.0, "description":"huawei is an affordable android phone"},
        { "id":3,  "title":"apple macbook",             "tags":"macosx laptop premium",     "price":1500.0,"description":"apple macbook is the top laptop in the market"},
        { "id":4,  "title":"hp spectre",                "tags":"windows laptop premium",    "price":1500.0,"description":"hp is spectre is a premium laptop, that is compatible with phones"},
        { "id":5,  "title":"lenovo ideapad white",      "tags":"windows laptop affordable", "price":400.0, "description":"lenovo idea pad is a white affordable product"},
        { "id":6,  "title":"iphone protection white",   "tags":"cover",                     "price":25.0,  "description":"protect iphone with a white phone cover"},
        { "id":7,  "title":"iphone protection leather", "tags":"cover",                     "price":25.0,  "description":"protect iphone with an amazing and premium leather phone cover"},
        { "id":8,  "title":"samsung s8 leather cover",  "tags":"cover",                     "price":25.0,  "description":"protect samsung s8 with an amazing and premium leather phone cover"},
        { "id":9,  "title":"generic acme product",      "tags":"",                          "price":100.0, "description":"this is a waterproof acme thingie for all sorts and kinds of people with lots of common words like find, amazing, market, you, your, in, with, the, feature, an and a"},
        { "id":10, "title":"generic acme product",      "tags":"",                          "price":100.0, "description":"this is a waterproof acme thingie for all sorts and kinds of people with lots of common words like find, amazing, market, you, your, in, with, the, feature, awesome, an and a"}
    ]

The impression table can be populated similarly. The POST request to /api/data/impressions/batch contains following kind of data:

    [
        { "prevProduct":null, "customer":0, "product":0, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":1, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":2, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":3, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":4, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":5, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":6, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":7, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":8, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":9, "query":"", "click":false},
        { "prevProduct":null, "customer":0, "product":10, "query":"", "click":false},
        ...
        { "prevProduct":0, "customer":7, "product":0, "query":"some protection for iphone", "click":false},
        { "prevProduct":0, "customer":7, "product":1, "query":"some protection for iphone", "click":false},
        { "prevProduct":0, "customer":7, "product":2, "query":"some protection for iphone", "click":false},
        { "prevProduct":0, "customer":7, "product":3, "query":"some protection for iphone", "click":false},
        { "prevProduct":0, "customer":7, "product":4, "query":"some protection for iphone", "click":false},
        { "prevProduct":0, "customer":7, "product":5, "query":"some protection for iphone", "click":false},
        { "prevProduct":0, "customer":7, "product":6, "query":"some protection for iphone", "click":false},
        { "prevProduct":0, "customer":7, "product":7, "query":"some protection for iphone", "click":true},
        { "prevProduct":0, "customer":7, "product":8, "query":"some protection for iphone", "click":false},
        { "prevProduct":0, "customer":7, "product":9, "query":"some protection for iphone", "click":false},
        { "prevProduct":0, "customer":7, "product":10, "query":"some protection for iphone", "click":false},
        { "prevProduct":null, "customer":8, "product":0, "query":"laptop", "click":false},
        { "prevProduct":null, "customer":8, "product":1, "query":"laptop", "click":false},
        { "prevProduct":null, "customer":8, "product":2, "query":"laptop", "click":false},
        { "prevProduct":null, "customer":8, "product":3, "query":"laptop", "click":true},
        { "prevProduct":null, "customer":8, "product":4, "query":"laptop", "click":false},
        { "prevProduct":null, "customer":8, "product":5, "query":"laptop", "click":false},
        { "prevProduct":null, "customer":8, "product":6, "query":"laptop", "click":false},
        { "prevProduct":null, "customer":8, "product":7, "query":"laptop", "click":false},
        { "prevProduct":null, "customer":8, "product":8, "query":"laptop", "click":false},
        { "prevProduct":null, "customer":8, "product":9, "query":"laptop", "click":false},
        { "prevProduct":null, "customer":8, "product":10, "query":"laptop", "click":false}
    ]

Note the '...'. Most of the data entries are omitted for brewity. It is normal for impression table to contain huge amounts of entries, as it basically captures behavioral data.

The undefined optional fields are marked with null. Notice, how the link values don't differ from other values.

#### 2.3 Stream and file data upload

*NOTE*: These functionalities should be considered to be in BETA phase. Report any issues to us, please.

The batch-API described above is restricted to payloads of 6MB and less. This can be limiting if there is a lot of data to upload, meaning that the data must be split
and serialised into suitable json-arrays. In order to fix this problem Aito (in version v158 and later), provides two improvements. The stream-upload API, and a set of
file upload APIs.

##### Stream API
The stream API, available under `/api/data/<table_name>/stream` will allow you to upload data similar to
the batch API, but without having to format it into a json array. The data should instead be formatted as
individual json elements separated by a newline. The lines will be parsed in FIFO order, much the same
as in th batch API, and stored into the database in a single operation.

##### File API
The file API, allows handling massive data sizes, limited by the database capacity, and to a lesser degree
by S3 size restrictions. The file API accepts data in the same format as the stream-API, but stored into
a file, which is uploaded and handled asynchronously by the database on a best effort basis.

The format of the uploaded file should be [newline delimited JSON or ndJson](http://ndjson.org), i.e. the 
individual json-elements separated by a newline character. Note that this is not the same as a JSON array, 
but rather individual elements. This is to allow upload size far exceeding the size of the size that can be 
kept in memory at any instant. The file should additionally *be gzipped* to reduce the size of the transferred
data. In summary, the file should 
1) be in ndjson-format
2) be gzipped before upload

The file API is not a single API, but requires at a minimum three calls (per table). The sequence is
as follows:

1. Initialise the upload process with a `POST` to `/api/data/<table_name>/file`. 
    ```bash
    curl -X POST https://environment.api.aito.ai/api/data/<table_name>/file
    ```
    The response is equivalent to this example (<sub>see [Aitoai test/demo env]( https://aitoai-test-env.api.aito.ninja/api-docs/v3/#/data/post_api_data__table__file),
    or at your own environment in the path `/api-docs/v3/#/data/post_api_data__table__file`.
    With this URL in the response you will be able to upload your data file</sub>): 
      ```json
      {
        "id": "9b38de74-0694-46ad-b239-1af5626f1fc9",
	    "url": "https://some-s3-bucket.s3.eu-west-1.amazonaws.com/theenv/thetable/9b38de74-0694-46ad-b239-1af5626f1fc9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20180730T142155Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1199&X-Amz-Credential=ACCESSKEY%2F20180730%2Feu-west-1%2Fs3%2Faws4_request&X-Amz-Signature=9fc31d825f9898b169a1a7747058778e0744a2052062afce6117b9127f1b3ed6",
	    "method": "PUT",
	    "expires": "2018-07-30T14:41:55"
      }
      ```
    * `id` is the job id, and is used to control the upload process
    * `url` is the S3 URL to which to upload the data file 
    * `method` is the only allowed method for the upload, currently always PUT
    * `expires` is the latest time when the upload can start. After that the upload URL expires and is no longer valid

2. Upload the file to S3, using the signed URL you received. The URL expires within 20 minutes. The upload can be done with any client, but in curl the command would be 
    ```bash
    curl -X PUT -T data_as.ndjson.gz "https://some-s3-bucket.s3.eu-west-1.amazonaws.com/theenv/thetable/9b38de74-0694-46ad-b239-1af5626f1fc9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20180730T142155Z&X-Amz-SignedHeaders=host&X-Amz-Expires=1199&X-Amz-Credential=ACCESSKEY%2F20180730%2Feu-west-1%2Fs3%2Faws4_request&X-Amz-Signature=9fc31d825f9898b169a1a7747058778e0744a2052062afce6117b9127f1b3ed6"
    ```
     The data is expected to be uploaded with `PUT`, and the data as the body of the message, not e.g. as a form-upload. 

3. Trigger the database process by a `POST` to the path `/api/data/<table_name>/file/<id>`
    ```bash
    curl -X POST https://environment.api.aito.ai/api/data/<table_name>/file/<id>
    ```
4. `GET` to the same URL will show the current progress of the operation. The status will show when
the process has been finished. 
    ```bash
    curl https://environment.api.aito.ai/api/data/<table_name>/file/<id>
    ```
    And the response, which shows you the progress, as well as the last failing rows (max 50)
    ```json
    {
      "status": {
        "finished": true,
        "completedCount": 20,
        "lastSuccessfulElement": {
          "primaryName": "Ania Josse",
          "birthYear": null,
          "nconst": "nm123456",
          "deathYear": null,
          "primaryProfession": "actress,miscellaneous"
        },
        "startedAt": "20180730T172233.473+0300"
      },
      "errors": {
        "message": "Last 0 failing rows",
        "rows": null
      }
    } 
    ```


#### 2.4 Seeing the current schema

Aito will display the current schema by issuing a GET request to `/api/schema`. The schema for an individual table can also be displayed by GET to the path `/api/schema/table_name`. 
The schema is equivalent to the provisioned schema, but ordering of the keys are not necessarily preserved. 

#### 2.5 Reseting the schema and deleting individual tables

Aito lets you reset the entire database, by sending DELETE request to `/api/schema/` (The previous path `/api/schema/_mapping` still works but is deprecated and will be removed)

Individual tables can be dropped by sending sending a DELETE request to the table URLS like `/api/schema/users`, `/api/schema/products` or `/api/schema/impressions`. 
For example sending DELETE to `/api/schema/users` will both erase the schema, and delete all the data in the table. After the operation, you can redefine the schema and repopulate the new version of the table.

#### 2.6 Managing data entry modifications

Aito doesn't currently support updating/deleting individual rows, all thought support for this is under development.

Still, it is a good question, whether updates/deletes are always desirable in case of Aito-like intelligence layer. Consider a situation, where the information about a product changes. Let's say, that we update following row.

    { "id":5,  "title":"lenovo ideapad white",      "tags":"windows laptop affordable", "price":400.0, "description":"lenovo idea pad is a white affordable product"},

This product information entry would be updated with following information.

    { "id":5,  "title":"lenovo ideapad white (-25% discount)",      "tags":"windows laptop affordable discount", "price":300.0, "description":"lenovo idea pad is a white affordable product"},

Now, if the entry gets updated, the old impressions will be relinked to the new entry. Yet, the impressions didn't occur for an item with -25% discount at 300.0 price point, but on an item with normal price at 400.0 euros. Making such a hard update on the laptop, would lead to falsification of history, and false impression statistics. Fundamentally, the different product entries are semantically different things.

This problem can be solved by having separate product information entries for the different product versions. This can be done by updating the schema to let us maintain several versions of the same products:

    {
        "table": "products",
        "columns": [
             { "column": "id",          "type": "Int" },
             { "column": "productId",   "type": "Int" },
             { "column": "active",      "type": "Boolean" },
             { "column": "title",       "type": "String",  "analyzer": "English" },
             { "column": "tags",        "type": "String",  "analyzer": "Whitespace" },
             { "column": "price",       "type": "Decimal", "analyzer": "No" },
             { "column": "description", "type": "String",  "analyzer": "English" }
         ]
     }

In this case, we may have

    { "id":5, "productId":5, "active":true, "title":"lenovo ideapad white",      "tags":"windows laptop affordable", "price":400.0, "description":"lenovo idea pad is a white affordable product"},

This product information entry would be updated with following information.

    { "id":11, "productId":5, "active":true, "title":"lenovo ideapad white (-25% discount)",      "tags":"windows laptop affordable discount", "price":300.0, "description":"lenovo idea pad is a white affordable product"},

The new impressions to the product would link the new impression id 11, while the old impressions keep linking the old version 5. This technique leads to duplicate product entries. The older product entries can be filtered using the 'active' column, when e.g. providing product recommendations:

    {
      "context": "impressions",
      "q" : [
        {"when":"customer", "is":5},
        {"find":"product"},
        {"where":"active", "is":true},
        {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

The problem here is that, aito does not suppport updating the old entry. To make this work, it should be possible to set the active flag to false in the old product line:

    { "id":5, "productId":5, "active":false, "title":"lenovo ideapad white", "tags":"windows laptop affordable", "price":400.0, "description":"lenovo idea pad is a white affordable product"},

### 3. Using aito ###

Following examples are based on the previous 'ecommerce' dataset. You can try the examples by populating aito with the ecommerce data, and by sending the example requests to your aito instance.

The ecommerce dataset and example code is available at [http://TODO]

#### Estimating

aito let's you try to estimate the likelihood of a feature based on the known information.

Consider the example:

    {
     "context": "impressions",
     "q" : [
       {"when":"product.title", "is":"apple iphone"},
       {"estimate":"click", "has":true}
      ]
    }

Aito will respond with a probability estimate of form:

    {
      "estimation" : 0.18556578014934794
    }

The probability is significantly higher than the expected 0.9 (for 11 items), which implies that aito believes the apple iphone ctr to be nearly twice to normal for the given mock data.

#### Predicting

Predicting is used to predict field's content.

Consider the following JSON, sent to https://aito-host/api/_predict end-point:

    {
      "context": "impressions",
      "q" : [
        {"when":"customer", "is":5}
       ],
       "predict":"query"
    }

Aito will respond with a list of predictions of form:

    [ {
        "probability" : 0.22107382504756828,
        "variable" : {
          "feature" : "best",
          "field" : "query",
          "id" : ".query:best",
          "priorW" : 2.0,
          "priorP" : 0.5
        }
      }, {
        "probability" : 0.20182963857124123,
        "variable" : {
          "feature" : "appl",
          "field" : "query",
          "id" : ".query:appl",
          "priorW" : 2.0,
          "priorP" : 0.5
        }
      },
      ...
    ]

In this example, we try to guess the query the customer might make and provide these as smart
recommendations. This technique can be used to provide better user experience, by proposing likely queries based on common queries and customer's old queries and choices.

Predictions are exclusive by default. The exclusiveness means, that if e.g. category contains the feature 'laptop' aito assumes that the category cannot contain the feature 'phone', or any other feature.

Following JSON demonstrates non-exclusive prediction, send to /api/_predict end-point:

    {
        "context": "products",
        "q" : [
            {"when":"title", "is":"huawei honor leather protection"},
            {"when":"description", "is":"protect your phone with an amazing and premium leather phone cover"}
        ],
        "exclusive":false,
        "predict":"tags"
    }

Such predictions can be used in the product information management (PIM) to make product tagging faster
and more consistent.

    [
      {
        "probability" : 0.9773316611646438,
        "variable" : {
          "feature" : "cover",
          "field" : "tags",
          "id" : ".tags:cover",
          "priorW" : 2.0,
          "priorP" : 0.5
        }
      },
      ...
    ]

#### Relating

Relating is an aito operation, that can be used to provide analytics and insights from the data.
The relating operation returns a set of relations, that can be used to provide analytics.

For example, we may post following JSON to "https://aito-host/api/_relate" end-point:

    {
      "context": "impressions",
      "q" : [
        {"when":"click", "has":true}
       ],
       "to":["product.title"]
    }

The relate method will relate the click is true event with the product's title-field features. The call will lead to following kind of output:

    [ {
        "mutualInformation" : 0.00649973689378424,
        "samples" : 253,
        "variables" : [ {
          "feature" : true,
          "field" : "click",
          "frequency" : 20,
          "conditional" : [ {
            "negationLift" : 1.016326918876258,
            "frequency" : 12,
            "samples" : 207,
            "whenVariableStates" : [ {
              "index" : 1,
              "state" : false,
              "field" : "product.title",
              "feature" : "appl"
            } ],
            "lift" : 0.8180714753788391,
            "probability" : 0.06737059209002204
          }, {
            "negationLift" : 0.9386445429910274,
            "frequency" : 8,
            "samples" : 46,
            "whenVariableStates" : [ {
              "index" : 1,
              "state" : true,
              "field" : "product.title",
              "feature" : "appl"
            } ],
            "lift" : 1.6836750923856951,
            "probability" : 0.13865559584352782
          } ],
          "index" : 0,
          "probability" : 0.08235294117647059,
          "variableId" : "click:true"
        }, {
          "feature" : "appl",
          "field" : "product.title",
          "frequency" : 46,
          "conditional" : [ {
            "negationLift" : 1.0138744473678438,
            "frequency" : 38,
            "samples" : 233,
            "whenVariableStates" : [ {
              "index" : 0,
              "state" : false,
              "field" : "click",
              "feature" : true
            } ],
            "lift" : 0.9374489454380646,
            "probability" : 0.17018646557888162
          }, {
            "negationLift" : 0.838562527314939,
            "frequency" : 8,
            "samples" : 20,
            "whenVariableStates" : [ {
              "index" : 0,
              "state" : true,
              "field" : "click",
              "feature" : true
            } ],
            "lift" : 1.727818838079856,
            "probability" : 0.3136718886339054
          } ],
          "index" : 1,
          "probability" : 0.18154211640758147,
          "variableId" : "product.title:appl"
        } ],
        "naiveEntropy" : 1.0938551664181706,
        "entropy" : 1.0806669914428908
      },
      ...
    ]

The output contains several mathematical metrics. One of the most important metric is the mutual information, which aito uses for measuring the strength of the statistical relationship between the variable and to sort the results. You can read more about this metric here:

[Mutual Information in Wikipedia](https://en.wikipedia.org/wiki/Mutual_information)

The relation lists the variables included in the relation. The list includes information about each variables statistical properties. As we can see in the list, the first entry is a relation between click:true-feature and product.title:appl-feature. The click:true feature's entry's probability-field describes the probability P(click:true) that is 0.08235294117647059.

Each relation variable entry has also a set of 'conditional' entries, which describe the variable under relevant conditions. The first entry for the click describes the conditional variable (click:true|product.title:appl = false), which probability P(click:true|product.title:appl = false) = 0.06737059209002204. This implies, that if the product doesn't have 'apple' on it, the click likelihood is lower, than the average P(click:true) = 0.08235294117647059.

The second entry describes the click likelihood, if the apple-feature is present. The likelihood is 0.13865559584352782, which 1.6836750923856951 times higher than the base probability. The 'multiplier' is captured in the 'lift' property.

As a tip: you probably want to use the lift values to describe cross-feature dependencies. E.g. if lift is 1.68 for apple-feature, you can say that 'people click Apple products 70% more often than average' or if lift is 0.81 you may say people that 'people click non-Apple products 20% less often than average'. Alternatively, you may simply say that e.g. 'product discount makes click 1.7 times more likely'. These kind of expressions may make the statistical metrics more comprehensible.

The Json response also contains some additional entries and metrics, which may, or may not be interesting.  Let's consider arelation R consisting of variables click:true and product.discount:true

* The relation's *naiveEntropy* is simply the sum of the variable entropies. If the entropies of click:true and product.discount:true are 0.1, the relation's 'naive entropy' is 0.2.
* Relation's entropy describes the estimated 'real entropy' of the relationship H(R) = H((click:true, product.discount:true)). This entropy describes the minimum amount bits, that the variables A, B, ... N can be compressed on a row level, when examined together. This 'real entropy' is always either equal or lower than the 'naive entropy'. If the variables are independent, the relation's entropy is equal to its naive entropy. If the real entropy is lower, it means that variables are dependent.
* The mutual information is the difference between the naive entropy and real entropy. E.g. let's say, that click:true and product:discount:true share 0.01 bit of information. This means, that the relation's entropy is 0.19, while the naive entropy is 0.2 and the mutual information is 0.01. This also implies, that if the dicount variable is known, the click variable has only H(click:true|product.discount:true)=0.09 bits of entropy. Having 10 perfectly independent 'teller' variables/relationships, that share 0.01 bits of information with the click variable would drop the click variable condititional entropy to 0, which would mean that the click variable could always be perfectly predicted, when the teller variables are known. Mutual information is useful for measuring the strenght of discrete variables' statistical relationship, because...
    * ...with discrete variables traditional metrics like correlations don't work very well
    * ...mutual information is extremely fast to calculate for binary features
    * ...mutual information is pretty much the mathematically correct metric to use and it has a clean theorethical interpretation as the geometric mean lift (gml(A;B)=2^I(A:B)) in the total likelihood of events
    * ...and it behaves linearly so that 0.01 bits of mutual information from one source and 0.01 bits of mutual information of another brings (at most) 0.02 bits of mutual information together. For example, the mutual informations probabilistic version 'geometric mean lift' doesn't have this property.
* The negationLift property of the conditional variable A under condition C is the value p(!A|C)/p(!A), while lift is the value p(A|C)/p(A)


##### More on relate

You can parametrize the relate operation in following way:

    {
      "context": "impressions",
      "q" : [
         {"when":"query"}
      ],
      "to":["product", {"field":"click", "is":true}],
      "from":10,
      "take":10,
      "filter":0.0001
    }

The from and size arguments can be used to pagination.

Filter provides the lowest relative mutual information returned relations must have. Relative mutual information is counted by dividiving the mutual information with each variables entropy. E.g. if 
variable A has entropy of 0.5 and the relation has mutual information of 0.1, the relative mutual 
information for variable A is 0.20. If any of the variables have relative mutual information above
the given threshold, the relation is returned. 

Note, that the relation retrieval is heavily optimized and not all relations are not checked for 
statistical significance. It is possible, that aito does not recognize some exclusive relationships
(e.g. if A is true, B is always false) because these optimizations. We seek to provide separate optimizations
for better detecting significant exclusive relationships later.

You can also use the 'variable' and 'and' operations to provide make more sophisticated examinations. In the following example, we are essentially asking for a single relation between 3 variables, of which 2 are composite variables formed with 'and'-operation. Filter is set to 0.0 to make aito return the relation
even if the relation were statistically insignificant.

    {
      "context": "impressions",
      "q" : [
        {
          "when":{
            "variable":{
              "and":[
                {"field":"query","is":"laptop"},
                {"field":"customer.tags","is":"20s"}
              ]
            }
          }
        }
      ],
      "to":[{"variable":{
               "and":[{"field":"product.tags", "is":"phone"},
                      {"field":"product.tags", "is":"premium"}]}
            },
            {"field":"click", "is":true}],
      "take":1,
      "filter":0.0
    }


#### Traditional queries

Find operation is used to provide traditional SQL-like queries, that return rows. Find operations support both filtering of the rows, and sorting the rows by given property.

For example, we may POST following JSON to "https://aito_host/api/_find" end point:

     {
        "context": "products",
        "q" : [
           {"find":{}},
           {"whereHit":"tags", "has":"phone"}
        ]
      }

The query returns all the phone products.

You can page the results in following way:

     {
        "context": "products",
        "q" : [
           {"find":{}},
           {"whereHit":"tags", "has":"phone"},
           {"from":1, "take":1}
        ]
      }

Sort the results:

     {
        "context": "products",
        "q" : [
           {"find":{}},
           {"whereHit":"tags", "has":"phone"},
           {"orderBy": "name", "desc": true},
           {"from":0, "take":100}
        ]
      }

Note, that you can filter results using the linked values, but aito transforms all linked values
into option types. It means, that the 'is' queries have to do in following way.

     {
        "context": "impressions",
        "q" : [
           {"find":{}},
           {"whereHit":"customer.tags", "is":{"option":"nyc 20s"}}
        ]
      }

This mechanism can be used to list all the broken links like this:

     {
        "context": "impressions",
        "q" : [
           {"find":{}},
           {"whereHit":"product.id", "is":{"option":{}}}
        ]
      }

If you want to get items from e.g. several categories, you can do it using or-operation like this:

    {
      "context": "products",
      "q" : [
         {"find":{}},
         {"whereHit":
           {"or":[
             {"field":"tags", "has":"phone"},
             {"field":"tags", "has":"cover"}
           ]}
         }
      ]
    }

Or operation can also be used to get items with specific ids:

    {
      "context": "products",
      "q" : [
         {"find":{}},
         {"whereHit":
           {"or":[
             {"field":"id", "is":1},
             {"field":"id", "is":2}
           ]}
         }
      ]
    }

##### Find-queries with 'columns'-filtering

Find queries, as presented above allow filtering rows based on field value or context. It is also possible
to filter the returned columns to reduce the size of the data. This allows reducing the number of columns
to contain merely the ones, which are relevant to the given query.

Column-filters can be applied as inclusive, using '_columns_' or exclusive, using '_excludeColumns_'. Please
note that inclusive filter '_columns_' limits the returned columns to include **only the columns in the list**.
Excluding filtering '_excludeColumns_' will on the other hand remove the columns specified in the list, but
return all others.

Example. Given the product catalog above, we can query the name and price of the products by the following query (TODO: add a link to the example above):

    {
       "context": "products",
       "q" : [
           {"find":{}},
           {"columns": ["title", "price"]}
       ]
    }

which would return all the products with their respective price:

    {
      "example": "json"
    }


Excluding columns would yield

    {
       "context": "products",
       "q" : [
           {"find":{}},
           {"excludeColumns": ["title", "price"]}
       ]
    }

returning

    {
      "example": "json"
    }



TODO do we want to expose:
  * Distinct values
    * what happens if we have a large number of columns to run distinct for
    * what happens if you do distinct select on a field, e.g. price? How does the other columns operate?
  * Over-a-link-fetching when running the query
  * Select for result types, rather than just value types?

#### Similarity / search

You can find items based on IDF similarity metrics by sending following kind of requests to /api/_find end-point:

    {
       "context": "messages",
       "q" : [
           {"find":{}},
           {"orderByHitProbability":{"similarity":{"field":"prev.message", "is":{"option":"how can I order a sim card ?"}}}}
       ]
    }

The result is following:

    {
      "count" : 25,
      "last" : 24,
      "totalCount" : 25,
      "first" : 0,
      "rows" : [ {
        "rowIndex" : 3,
        "hitIndex" : 0,
        "row" : {
          "id" : 3,
          "message" : "click co fi sim to order the card",
          "user" : 0,
          "prev" : 2
        },
        "probability" : 0.2872893041731717
        },
        ...
      }

NOTE: You can filter items based on linked columns, but keep in mind that aito transforms all linked
values into optional values. This mechanism is there, because the symbolic links may be broken,
so aito cannot guarantee that there is a value behind the link.

This means, that when sorting impressions based on the linked products, instead of using following
kind of query

    {
      "context": "impressions",
      "q" : [
        {"find":[]},
        {"orderByHitProbability":
           {"similarity":{"field":"product.title", "is":"apple iphone"}}
        },
        {"take":10, "from":0}
      ]
    }

You have to use an 'option' value like this:

     {
      "context": "impressions",
      "q" : [
        {"find":[]},
        {"orderByHitProbability":
           {"similarity":{"field":"product.title", "is":{"option":"apple iphone"}}}
        },
        {"take":10, "from":0}
      ]
    }

If you want to compare several fields, you can use 'and' query in the following way in the products
table:

    {
      "context": "products",
      "q" : [
        {"find":[]},
        {"orderByHitProbability":{
          "similarity":{
            "and":[
              {"field":"title", "is":"apple iphone"},
              {"field":"tags", "is":"premium phone"}
            ]
          }
        }},
        {"take":10, "from":0}
      ]
    }

If you want to compare several fields so that each field is normalized separately, you 
can use following kind of query:

    {
      "context": "products",
      "q" : [
        {"find":[]},
        {"orderByHitScore":
          {"product":[
             {"lift": {"similarity": {"field":"title", "is":"apple iphone"}}},
             {"lift": {"similarity": {"field":"tags", "is":"premium phone"}}}
          ]}
        },
        {"take":10, "from":0}
      ]
    }

Lift-operation turns the otherwise 0-1 probability-like value into a 1-normalized probability multiplier, where lift 2.0 implies the item to be twice as likely, and 0.5 the item to be half as likely.

If you want to emphasize certain fields in results, you can use exponent. If you want then to normalize
the results to look like probabilities, you can use sumTo-operation in the following way: 

    {
      "context": "products",
      "q" : [
        {"find":[]},
        {"orderByHitScore":
          {"sumTo":1.0,
           "score":{
             "product":[
               {"exponent":2.0, "base":{"lift": {"similarity": {"field":"tags", "is":"phone"}}}},
               {"lift": {"similarity": {"field":"description", "is":"phone"}}}
             ]
           }
          }
        },
        {"take":10, "from":0}
      ]
    }

#### Matching / Find by likelihood

You can find matches with following kind of requests, sent to /api/_find end-point:

    {
      "context": "messages",
      "q" : [
         {"when":"prev.message", "is":"how can I order a sim card ?"},
         {"find":{}},
         {"orderByHitProbability":"message"}
      ]
    }

With the customerService test data, this returns:

    {
      "count": 10,
      "totalCount": 100,
      "rows": [
        {
          "rowIndex" : 8,
          "hitIndex" : 0,
          "row" : {
            "id" : 8,
            "message" : "click this co fi sim to order the sim",
            "user" : 0,
            "prev" : 7
          },
          "probability" : 0.9829291525481283
        }, {
          "rowIndex" : 3,
          "hitIndex" : 1,
          "row" : {
            "id" : 3,
            "message" : "click co fi sim to order the card",
            "user" : 0,
            "prev" : 2
          },
          "probability" : 0.01697314828924152
        },
        ...
      ]
    }


#### The triangle (context-decision-KPI) examination

The 'triangle examination' is a statistical optimization mechanism, that powers smart search, personalization, matching and numerous other aito use cases. This examination is all about
finding a decision that optimizes a KPI in the decision context.

Using the aito query language terms the problem is to "find Decision by KPI when Context"

Consider following example:

    {
      "context": "impressions",
      "q" : [
        {"when":"customer", "is":5},
        {"find":"product"},
        {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

In the example, we are facing a customer (our decision context), and our problem is to select a product (our decision), that the customer will buy (our KPI).

We can also reverse the problem setting:

    {
      "context": "impressions",
      "q" : [
         {"when":"product", "is":4},
         {"find":"customer"},
         {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

In this problem setting we have a product (our decision context), and our problem is to find customers (our decision) likely to buy the product (KPI). The query will essentially provide us a call list of the most potential customers.


##### The triangle examination: Smart search

aito has an analogy mechanism, that enables smart search operations.

Consider the JSON, sent to the the /api/_find end-point:

    {
      "context":"impression",
      "q" : [
        {"when":"query", "is":"laptop"},
        {"find":"customer"},
        {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

This will find the laptop with the highest CTR. The aito response is following:

    [{
      "rowIndex" : 3,
      "hitIndex" : 0,
      "row" : {
        "description" : "apple macbook is the top laptop in the market",
        "tags" : "macosx laptop premium",
        "price" : 1500.0,
        "id" : 3,
        "title" : "apple macbook"
      },
      "probability" : 0.8241595816690801
    }, {
      "rowIndex" : 4,
      "hitIndex" : 1,
      "row" : {
        "description" : "hp is spectre is a premium laptop, that is compatible with phones",
        "tags" : "windows laptop premium",
        "price" : 1500.0,
        "id" : 4,
        "title" : "hp spectre"
      },
      "probability" : 0.08141562856029637
    }, {
      "rowIndex" : 5,
      "hitIndex" : 2,
      "row" : {
        "description" : "lenovo idea pad is an affordable white laptop",
        "tags" : "windows laptop affordable",
        "price" : 400.0,
        "id" : 5,
        "title" : "lenovo ideapad white"
      },
      "probability" : 0.04249644599142157
    },
    ...
    ]

Aito will return also non-laptop results, even if such results' probabilities are lower.

Consider the JSON, sent to the the /api/_find end-point:

    {
      "context":"impression",
      "q" : [
        {"when":"query", "is":"cheap laptop"},
        {"find":"customer"},
        {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

So the query is for cheap laptop, which prompts following response:

    [ {
        "rowIndex" : 5,
        "hitIndex" : 0,
        "row" : {
          "description" : "lenovo idea pad is an affordable white laptop",
          "tags" : "windows laptop affordable",
          "price" : 400.0,
          "id" : 5,
          "title" : "lenovo ideapad white"
        },
        "probability" : 0.5063379712706078
      }, {
        "rowIndex" : 3,
        "hitIndex" : 1,
        "row" : {
          "description" : "apple macbook is the top laptop in the market",
          "tags" : "macosx laptop premium",
          "price" : 1500.0,
          "id" : 3,
          "title" : "apple macbook"
        },
        "probability" : 0.2677799274177719
      }, {
        "rowIndex" : 2,
        "hitIndex" : 2,
        "row" : {
          "description" : "huawei is an affordable android phone",
          "tags" : "android phone affordable",
          "price" : 150.0,
          "id" : 2,
          "title" : "huawei honor"
        },
        "probability" : 0.12041470684457642
      },
      ...
    ]

Now the affordable laptop is at the top position. Note that none of results contain the word 'cheap'. Instead, aito has learned that cheap refers to refer to certain keywords (like 'affordable'), and specific products.

It is worth noting, that even when 'aito' can find word matches to any query, there is no need to specify the 'query' field as a query field. aito will recognize automatically, that query feature X is often matched by same feature X in title field and less often by the same feature X in the description field. Still, the automation requires some example data (e.g. tens or hundreds of example queries) with clear X-to-X matching patterns to activate.

This word matching mechanism activates automatically for any smart operations, where any X-to-X
patterns are present. The relationship can be positive or negative.

##### The triangle examination: content personalization

Personalization is simply one way to use the triangle examination.

    {
      "context": "impressions",
      "q" : [
         {"when":"customer", "is":5},
         {"find":"product"},
         {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

By including previous events, like previous clicks and previous buys, you can bring some
collaborative elements to the otherwise content-based recommendations.

##### The triangle examination: "related products"-recommendations

Aito can find related products, that relate to the examined product, if you can find aito the right data.

One way to provide product-to-product recommendations is simply by keeping a score of what product customer clicked before. Another way can be e.g. by keeping a score of the last item(s) bought, or put into the shopping basket.

The ecommerce example contains 'prevProduct' field without specifying its correct semantics. By storing the previously clicked, previously added, or previously bought content, you can try to guess your customer's next intents.

Anyway, the previous product information can be used by sending following kinds of queries into the /api/_find end point:

    {
      "context": "impressions",
      "q" : [
         {"when":"prevProduct", "is":{"option":0}},
         {"find":"product"},
         {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

With the Ecommerce mock data, aito returns following results:

    [ {
        "rowIndex" : 6,
        "hitIndex" : 0,
        "row" : {
          "description" : "protect iphone with a white phone cover",
          "tags" : "cover",
          "price" : 25.0,
          "id" : 6,
          "title" : "iphone protection white"
        },
        "probability" : 0.5967635351330544
      }, {
        "rowIndex" : 7,
        "hitIndex" : 1,
        "row" : {
          "description" : "protect iphone with an amazing and premium leather phone cover",
          "tags" : "cover",
          "price" : 25.0,
          "id" : 7,
          "title" : "iphone protection leather"
        },
        "probability" : 0.2969097549886795
      },
      ...
    ]

The mock data contains clear statistical links between iPhone, and iPhone covers, which aito picks up easily.

##### The triangle examination: smart notifications

By reverting the examination, you can find quickly a list of users, who might be interested
of the new content:

    {
      "context": "impressions",
      "q" : [
         {"when":"product", "is":5},
         {"find":"customer"},
         {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

In this example, we are searching for users, who might be interested of the 'white lenovo idea pad'.

These kind of smart notifications may not work complete out of box, because aito easily assumes that people, who have bought the product, may want to buy the same product again. The problem with this assumption is that customers typically don't need products that they already have. This issue can be circumvented by keeping track of the customers impressions after they have bought specific products. Aito is able to recognize the pattern, where people, who just bought laptops are no more interested of laptop.

Now, if you make a new data type called 'customerState' with following kind of structure:

      {
        "table": "customersState",
        "columns": [
          { "column": "customer",       "type": "Int",    "link":"customers.id" },
          { "column": "recentlyBought", "type": "String", "analyzer":"Whitepace" },
          { "column": "prevBought0",     "type": "String", "link": "products.id" }
          { "column": "prevBought1",     "type": "String", "link": "products.id" }
          { "column": "prevBought2",     "type": "String", "link": "products.id" }
        ]
      }

The customer state would maintain links to the last few bought items, and it could have large
pack of product ids, what the customer had bought during the last 6-12 months.

Now if you store a link to these states in the impression log, following queries may deliver the desired results:

    {
      "context": "impressions",
      "q" : [
         {"when":"product", "is":5},
         {"find":"customerState"},
         {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

With enought data, aito may be able to reason, that the customer doesn't want another phone, because he recently bought a phone. At the same time, aito may be able to reason, that if you bought a large pack of coca cola before, you are likely to buy the same soon product again, and that you may be interested of new soda products and discounts.

As a disclaimer, we cannot quite know, that how aito will behave in different circumstances. Techniques like customerStates may or they may not work as intended.

##### The triangle examination: match making

The following example demonstrates aito in the context of dating:

    {
      "context": "impressions",
      "q" : [
         {"when":"user", "is":534534},
         {"find":"profile"},
         {"orderByContextProbability":{"field":"click", "has":true}}
      ]
    }

aito can use both personal id, and the profile information for inferring possible
matches. aito will also utilize word matches, and their statistical relationships
automatically. This enables aito to match people fond of 'movies' with people
of similar interests. It also lets aito to avoid matching a person who hates cats
with a person who loves cats.

##### Diagnostics - *Not yet implemented in the public, web-based API*

Currently: Aito support advanced diagnostics for find operations. Find operations
return a hits object, which can be used to access to the used scoring scheme explanation:

    val hits =
      impressions
        .when("user" is 4534)
        .find("product")
        .byProbability(\_ \ "click" is true)

    hits.scoring.explain match {
        case product:ProductScoringExplanation =>
            product.factors.foreach { case s : VariableStateScoringExplanation =>
                println("lift " + s.score + " for variable " + s.variableId + " state " + s.state)
            }
    }

The produced explanation describes the scoring components, that were used to produce individual scores.

It is also possible to explain individual hits by using the hit's explain method.

    impressions
      .when("user" is 4534)
      .find("product")
      .byProbability(_ \ "click" is true).foreach { hit =>
       hit.explain match {
           case product:ProductScoreExplanation =>
                product.factors.foreach { case s : VariableStateScoreExplanation =>
                println("lift " + s.score + " for variable " + s.variableId + " state " + s.state)
            }
       }
    }

## aito - the core concepts ##

aito is an synthesis of a database and AI technology, and as such, it introduces concepts and idea
from both of these realms. Some of database concepts and AI concepts have different names, yet are essentialy conceptually very similar or the same. In this chapter, we introduce the core concepts of the aito intelligence layer, and we describe how they relate to the terms existing elsewhere.

The basic concepts relating to the data are following:

* *Table*. Aito is a database like entity, that stores its data in relational tables consisting of rows and  columns, a bit like an SQL database.

* *Schema*. Each table has a strict schema, as in an SQL database. The schema contains the column names and value types, plus additional metainformation about linking, and the analysis of column values.

* *Values*. The table - of course - contains values at the intersection of each row and column. Strings, Integers, Long integers and double precision floating points are supported, as well as their optional versions.

* *Analyzers* and *features*. Aito does do the statistical reasoning at the level of values, but at reasons at the level of 'binary features'.

    * Specialized analyzers are used to decompose potentially large values (e.g. long text values) into individual binary features.
    * For example, if field 'age' has a value 54, this can be turned into feature age:54. If a field 'text' value 'horses are running' is analyzed with English language analyzer, it can be turned into features text:'hors' and text:'run'. The analyzers do not just split the text into words, but it can also drop overly common words, and reduce the words into their normal forms.
    * Analyzers may be familiar from search engines like Lucene or its derivatives like ElasticSearch and Solr.
    * While the main data is stored in a table, the features are stored in a separate feature dataframe

The concepts related to the use of aito are following:

+ Context. All aito's operations are done in a context. Context define a kind of a scenario, in which we there are knowns, and there unknowns.
    * Context has always some examined 'root table'. For example: this table may be the impression table.
    * Context has some knowns. For example, in the impression context, we may know the user,
      and we may know the page the user is int.
    * The context often has unknowns, which we haven't specified. For example, we may not know, whether
      the displayed content was clicked.

+ Relation. in aito, relation refers to the statistical examination of a set of features.
    * For example, the relation (name:'playstation', productCategory:electronics') is used to
      examine playstation's statistical relationship with the 'electronics' product category.
    * Also, the relation (user:'bob', product.category:'laptop', click:true) can be used to
      examine Bob's preference for laptops.
    * Aito often examines hundreds or thousands of relationships per query to find interesting
      statistical relationships that may help to provide results.



### Future operations

In future, aito will provide smart highlights, and OR operations.

## Quality, scaling and known issues ##

Following functionality is well tested:

+ Prediction functionality is pretty fast, and it provides proper results. Data points are following
    * In DNA classification problem, predict provides 3%-5% error rate with 1ms-5ms prediction speed. 4k train samples, 450 features
    * In spam detection problem, predict provides 7% error rate with 1ms-5ms predictions speed. 5.5k train samples, 8k featurs
    * In personality detection problem, predict provided 83% accuracy with about 15ms-20ms response speed. 8.5k trains amples. 130k features.
+ find(x).byP(\_.context \ "X" \ "Y" ) is similarly fast, and it provides sensible results
    * in a generated personalization test, predict learned the underlying preference patterns, and scored 250k items in 20ms based on 1M samples and 4k features.

### Known issues
+ Please report any bugs you find.
